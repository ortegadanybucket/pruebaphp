
# Examen Práctico de Programación PHP

### Dany Ortega
### CC 109379575

Proyecto con tecnología MVC y servicios web REST Desarrollado en Laravel 7 integrado con Vue Js

## Requerimientos del Proyecto

- Crear una orden para un cliente con una cantidad  máxima de 5 productos. por ejemplo, si un cliente ordena del producto A 3 unidades y del producto B 4 unidades, en la misma orden, la cantidad total será 7 por lo tanto no debe permitir crear la orden. Tenga en cuenta que sólo algunos productos están permitidos por cliente, realizar las validaciones correspondientes

- Listar las órdenes de un cliente por un rango de fechas.

## Requerimientos de Instalacion

- tener instalado y configurado servidor web para php


## Pasos para instalacion:

- Clonar el proyecto desde el siguiente repositorio: $ git clone https://ortegadanybucket@bitbucket.org/ortegadanybucket/pruebaphp.git
- Dentro del directorio de la aplicacion tener permisos de escritura sobre la carpeta storage $ sudo chmod -R 755 storage
- Dentro del directorio de la aplicacion Instalar dependencias con composer $ composer install
- Instalar Dependencias node $ sudo npm install
- Ejecutar php artisan cache:clear y php artisan config:cache en el directorio
- Ejecutar php artisan serve

## Ejecutar

### Parte 1:

Para probar los servicios REST URL: http://DOMAIN_PATH/api/orders

- Metodo GET Listar y Filtrar Ordenes por cliente y fecha de creacion
- Metodo POST Crear una nueva Orden

### Parte 2

Para probar la pagina que consume el metodo GET del API:

- Ingresar a la pagina principal del sitio  URL: http://DOMAIN_PATH/

## Documentacion

En la carpeta docs del repositorio se encuentra:

- Diagrama de clases.
- Archivo JSON con peticiones para importar a POSTMAN.
- Mayor documentacion del API en el siguiente link: https://documenter.getpostman.com/view/7333823/SzmcZybq


