<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('head')
    </head>
    <body>
        <div>
            <div class="content m-5">
                <div class="row pb-2 mt-4 mb-2 border-bottom" id="header-band">
                    <div class="col-8">                        
                        <h1>Examen Práctico de Programación PHP</h1> 
                        Dany Ortega 
                        </br>
                        CC 1093796575
                    </div>
                    <div class="col-4">                        
                        <img src="images/beitech_logo.png" class="float-right" height="60">
                    </div>   
                </div>
                <div class="row" id="app">
                    <orders-list-component></orders-list-component>
                </div>
            </div>
        </div>
        @include('footer-scripts')
    </body>
</html>
