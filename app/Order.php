<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'order';
    protected $primaryKey = 'order_id';
    public $timestamps = false;

    protected $casts = [
		'customer_id' => 'int',
		'total' => 'float'
	];

    protected $dates = [
		'creation_date'
	];

	protected $fillable = [
		'customer_id',
		'creation_date',
		'delivery_address',
		'total'
	];

	protected $with = array('customer', 'order_details');

	public function customer()
	{
		return $this->belongsTo(Customer::class, 'customer_id', 'customer_id');
	}

	public function order_details()
	{
		return $this->hasMany(OrderDetail::class, 'order_id', 'order_id');
	}
}
