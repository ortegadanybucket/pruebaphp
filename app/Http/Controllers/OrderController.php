<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Customer;
use App\OrderDetail;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $tableFilters = [''];

    public function index(Request $request)
    {
        $validator =  Validator::make($request->all(),[
            'customer_id' => 'integer',
            'date_from' => 'date_format:Y-m-d',
            'date_to' => 'date_format:Y-m-d'
            ]);
        
        if($validator->fails()){
            return response()->json([
                "error" => 'validation_error',
                "message" => $validator->errors(),
            ], 422);
        }
        try{
            $orders = new Order;
            $orders = $orders->newQuery();
            if ($request->has('customer_id')) {
                $customerId = $request->input('customer_id');
                $orders->where('customer_id', $customerId);
            }
            if ($request->has('date_from') && $request->has('date_to')) {
                    $dateFrom = $request->input('date_from');
                    $dateTo = $request->input('date_to');
                    $orders->whereBetween('creation_date', [$dateFrom,$dateTo]);
            }
            $orders = $orders->get();     
        } catch (\Throwable $e) {
            return response()->json([
                "error" => 'server error',
                "message" => 'Error getting Orders.',
            ], 500);
        }
        catch (\Exceptions $e) {
            return response()->json([
                "error" => 'server error',
                "message" => 'Error getting Orders.',
            ], 500);
        }
        return response()->json($orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Crea una nueva orden considerando los siguiente
    //la suma de las cantidades en una orden no puede ser mayor a 5
    //Un cliente solo puede solicitar productos a los que este asociado
    public function store(Request $request)
    {
        $answer = "";
        $answerCode = 200;
        $validator =  Validator::make($request->all(),[
            'customer_id' => 'required|integer',
            'delivery_address' => 'required|string|max:191',
            'details' => 'required|array|min:1',
            'details.*.product_id' => 'required|integer|distinct',
            'details.*.quantity' => 'required|integer',
            ]);
        
        if($validator->fails()){
            return response()->json([
                "error" => 'validation_error',
                "message" => $validator->errors(),
            ], 422);
        }
        $total = 0;
        $totalQuantity = 0;
        DB::beginTransaction();
        try{
            $customer = Customer::find($request->input('customer_id'));
            if ($customer == null) {
                return response()->json([
                    "error" => 'Error',
                    "message" => 'Customer not exist not belong to the customer',
                ], 400);
            }

            $order = new Order;
            $order->customer_id = $customer->customer_id;
            $order->delivery_address = $request->input('delivery_address');
            $order->creation_date = now();
            $order->total = 0;
            $order->save();
            $errString = "";
            $customerProducts = $customer->products()->get();
            foreach ($request->input('details')  as $detail) {
                $product = $customerProducts->find($detail['product_id']);
                if ($product == null) {
                    $errString .= '- Product id '.$detail['product_id'].' not exist or not belong to customer';
                }else if($errString == ""){
                    $orderDetail = new OrderDetail;
                    $orderDetail->order_id = $order->order_id;
                    $orderDetail->product_id = $product->product_id;
                    $orderDetail->product_description = $product->product_description;
                    $orderDetail->price = $product->price;
                    $orderDetail->quantity = $detail['quantity'];
                    $orderDetail->save();
                    $total += $orderDetail->quantity*$orderDetail->price;
                }
                $totalQuantity += $detail['quantity'];
            }
            if($totalQuantity>5){
                $errString .= '- The sum of products quantity is greater than five(5)';
            }
            if($errString == ""){
                $order->total = $total;
                $order->save();
                DB::commit();
            }
        } 
        catch (\Throwable $e) {
            DB::rollback();
            $answerCode = 500;
            $answer = [
                "error" => 'server error',
                "message" => 'Error store order.',
            ];
            return response()->json($answer,$answerCode);
        }
        catch (\Exceptions $e) {
            DB::rollback();
            $answerCode = 500;
            $answer = [
                "error" => 'server error',
                "message" => 'Error store order.',
            ];
            return response()->json($answer,$answerCode);
        }  
        if($errString == ""){
            $answerCode = 200;
            $answer = Order::find($order->order_id);
        }else{
            DB::rollback();
            $answerCode = 400;
            $answer = [
                "error" => 'Error',
                "message" => $errString,
            ];
        }
             
        return response()->json($answer,$answerCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
