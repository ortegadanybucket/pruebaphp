<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    //
    protected $table = 'order_detail';
    protected $primaryKey = 'order_detail_id';
    public $timestamps = false;

    protected $casts = [
		'order_id' => 'int',
		'product_id' => 'int',
		'price' => 'float',
		'quantity' => 'int'
	];

	protected $fillable = [
		'order_id',
		'product_id',
		'product_description',
		'price',
		'quantity'
	];

	public function product()
	{
		return $this->belongsTo(Product::class);
	}

	public function order()
	{
		return $this->belongsTo(Order::class);
	}
}
