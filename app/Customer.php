<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = 'customer';
    protected $primaryKey = 'customer_id';
    public $timestamps = false;

    protected $fillable = [
		'name',
		'email'
	];

	public function products()
	{
		return $this->belongsToMany(Product::class,'customer_product', 'customer_id', 'product_id');
	}

	public function orders()
	{
		return $this->hasMany(Order::class);
	}
}
