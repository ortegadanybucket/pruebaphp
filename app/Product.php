<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'product';
    protected $primaryKey = 'product_id';
    public $timestamps = false;

    protected $casts = [
		'price' => 'float'
	];

    protected $fillable = [
		'name',
		'product_description',
		'price'
	];

	public function customers()
	{
		return $this->belongsToMany(Customer::class,'customer_product', 'product_id', 'customer_id');
	}

	public function order_details()
	{
		return $this->hasMany(OrderDetail::class);
	}
}
